#include<stdio.h>

void ins(int A[],int i)
{
  int k=i-1,t=A[i];
  while(k>=0 && A[k]>t)
  {
    A[k+1]=A[k];
    k--;
  }
  A[k+1]=t;
}

void inssort(int A[],int i)
{
  if(i>=1)
  {
    inssort(A,i-1);
    ins(A,i);
  }
}

int main()
{
  srand(time(0));
  int A[10050];
  int i,j,n;
  while(1)
  {
    printf("Enter size of array(upto 100000)");
    scanf("%d",&n);
    if(n>10000)
      continue;
    else
      break;
  }

  printf("generated array is\n");
  for(i=0;i<n;i++)
  {
    A[i]=rand()%1000000-rand()%100000+rand()%10000-rand()%1000+rand()%100-rand()%10;
    printf("%d\n",A[i]);
  }
  inssort(A,n-1);

  printf("Sorted array is\n");
  for(i=0;i<=n-1;i++)
  {
    printf("%d\n",A[i]);
  }

}
