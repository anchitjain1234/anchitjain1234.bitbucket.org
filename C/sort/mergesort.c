#include<stdio.h>

void merge(int A[],int p,int q,int r)
{
  int n1,n2;
  n1=q-p+1;
  n2=r-q;
  int l[n1],m[n2];
  int i,j=0,k=0,f=0,g=0;
  // f and g are flags to tell whether l and m arrays have been checked completely.

  for(i=0;i<n1;i++)
  {
    l[i]=A[p+i];
  }
  for(i=0;i<n2;i++)
  {
    m[i]=A[q+i+1];
  }

  for(i=p;i<=r;i++)
  {
    if(f==1)
    {
      A[i]=m[k];
      k++;
      continue;
    }
    if(g==1)
    {
      A[i]=l[j];
      j++;
      continue;
    }
    if(l[j]<=m[k])
    {
      A[i]=l[j];
      j++;
      if(j==n1)
        f=1;
    }
    else
    {
      A[i]=m[k];
      k++;
      if(k==n2)
        g=1;
    }
  }
}

void mergesort(int A[],int p,int r)
{
  if(p<r)
  {
    int q=(p+r)/2;
    mergesort(A,p,q);
    mergesort(A,q+1,r);
    merge(A,p,q,r);
  }
}

int main()
{
  srand(time(0));
  int A[10050];
  int i,j,n;
  while(1)
  {
    printf("Enter size of array(upto 100000)");
    scanf("%d",&n);
    if(n>10000)
      continue;
    else
      break;
  }

  printf("generated array is\n");
  for(i=0;i<n;i++)
  {
    A[i]=rand()%1000000-rand()%100000+rand()%10000-rand()%1000+rand()%100-rand()%10;
    printf("%d\n",A[i]);
  }
  mergesort(A,0,n-1);

  printf("Sorted array is\n");
  for(i=0;i<=n-1;i++)
  {
    printf("%d\n",A[i]);
  }
}
