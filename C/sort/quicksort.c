#include<stdio.h>

void generate(int A[],int n)
{
  srand(time(0));
  int i;
  printf("generated array is\n");
  for(i=0;i<n;i++)
  {
    A[i]=rand()%100;
    printf("%d\n",A[i]);
  }
}

void printarr(int A[],int n)
{
  int i;
  for(i=0;i<n;i++)
  {
    printf("%d\n",A[i]);
  }
}

void exchange(int A[],int a,int b)
{
  int temp;
  temp=A[b];
  A[b]=A[a];
  A[a]=temp;
}

int partition(int A[],int p,int r)
{
  int i,j,x,temp;
  i=p-1;
  x=A[r];
  for(j=p;j<=r-1;j++)
  {
    if(A[j]<=x)
    {
      i++;
      exchange(A,i,j);
    }
  }
  i++;
  exchange(A,i,r);
  return i;
}

void quicksort(int A[],int p,int r)
{
  int q;
  if(p<r)
  {
    q=partition(A,p,r);
    quicksort(A,p,q-1);
    quicksort(A,q+1,r);
  }
}


int main()
{
  int n,A[100];
  printf("Enter size of array\n");
  scanf("%d",&n);

  generate(A,n);

  quicksort(A,0,n-1);
  printf("Sorted array is\n");
  printarr(A,n);

}
