#include "functions.h"
int*** creategraph(char *file)
{
	int ***grap;
	int **graph;
	FILE *fp;
	fp=fopen(file,"r");
	
	int ver,edges;
	
	fscanf(fp,"%d %d",&ver,&edges);
	
	graph=(int **)malloc(ver*sizeof(int *));
	int i;
	for(i=0;i<ver;i++)
	{
		graph[i]=(int *)malloc(ver*sizeof(int));
	}
	int j;
	for(i=0;i<ver;i++)
	{
		for(j=0;j<ver;j++)
		{
			graph[i][j]=0;
		}
	}
	grap=&graph;
	int e1,e2;
	while(fscanf(fp,"%d %d",&e1,&e2)==2)
	{
		graph[e1-1][e2-1]=1;
		graph[e2-1][e1-1]=1;
	}
	
	fclose(fp);
	return grap;
}
