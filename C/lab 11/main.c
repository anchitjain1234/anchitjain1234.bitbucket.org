#include "functions.h"


void printadjgraph(int **graph,int ver)
{
	printf("Adjacency matrix is \n");
	int i,j;
	
	for(i=0;i<ver;i++)
	{
		for(j=0;j<ver;j++)
		{
			printf("%d ",graph[i][j]);
		}
		printf("\n");
	}
}


int getvertices(char* filename)
{
	FILE *fp;
	fp=fopen(filename,"r");
	int ver;
	fscanf(fp,"%d",&ver);
	fclose(fp);
	return ver;
}

int main(int argc,char *argv[])
{
	char filename[15];
	strcpy(filename,"input.txt");
	int ver;
	ver=getvertices(filename);
	int ***grap;
	
	grap=creategraph(filename); 
	
	int **graph;
	
	graph=*grap;
	
	printadjgraph(graph,ver);
		
	runbfs(graph,ver);
	//rundfs(graph,ver);
	
	return 0;
}
