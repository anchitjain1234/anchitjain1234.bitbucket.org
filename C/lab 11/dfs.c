#include "functions.h"
int si5=0;

int time=0;

int printstatus(node *nodes)
{
	printf("nodes after dfs\n");
	int i,fin=0;
	
	for(i=0;i<si5;i++)
	{
		printf("for vertice %d - parent = %d , ti = %d, tf = %d \n",i+1,nodes[i].parent+1,nodes[i].ti,nodes[i].tf);
		if(nodes[i].parent==-1)
		{
			fin++;
		}
	}
	return fin;
}
	
void dfsvisit(int u,int **graph,node *nodes)
{
	printf("%d ",u+1);
	nodes[u].color=2;
	time++;
	nodes[u].ti=time;
	int i,j;
	for(i=0;i<si5;i++)
	{
		if(graph[u][i]!=0 && nodes[i].color==1)
		{
			nodes[i].parent=u;
			dfsvisit(i,graph,nodes);
		}
	}
	nodes[u].color=0;
	time++;
	nodes[u].tf=time;	
}

void rundfs(int **graph,int ver)
{
	si5=ver;
	node* nodes;
	nodes=(node *)malloc(si5*sizeof(node));
	int i,j;
	for(i=0;i<si5;i++)
	{
		nodes[i].color=1;
		nodes[i].parent=-1;
	}
	
	printf("connected components \n");
	int fin=0;
	for(i=0;i<si5;i++)
	{
		if(nodes[i].color==1)
		{
			dfsvisit(i,graph,nodes);
			printf("\n");
			fin++;
		}
	}
	//printstatus(nodes);
	printf("Total connected componets are %d \n",fin);
}
