#include<stdio.h>
#include "header.h"	
node *h;
void printlist(node *h1)
{
  printf("Scanned elements are\n");
  node *h2=h1;
  while(h2)
    {
      printf("%d\n",h2->i );
      h2=h2->next;
    }
}

node * jump(node *A,int n)
{
  node *temp=A;
  while(n>0)
  {
    temp=temp->next;
    n--;
  }
  return temp;
}

void exchange(node *A,int a,int b)
{
  node *t1,*t2;
  int temp;
  t1=jump(A,a);
  t2=jump(A,b);
  temp=t1->i;
  t1->i=t2->i;
  t2->i=temp;

}

int takepivot(int p,int r)
{
    return r;
}

int partition(node *A,int p,int r)
{
  if (p==0)
  {
    h=A;
  }
  int i,j,x,pivot;
  node *temp,*a,*b;
  i=p-1;
  pivot=takepivot(p,r);
  temp=jump(A,pivot);
  x=(temp->i);
  for(j=p;j<=r-1;j++)
  {
    temp=jump(A,j);
    if((temp->i)<=x)
    {
      i++;
      exchange(A,i,j);
    }
    
  }
  i++;
  exchange(A,i,pivot);
  return i;
}

void quicksort(node *A,int p,int r)
{
  int q;
  if(p<r)
  {
    q=partition(A,p,r);
    quicksort(A,p,q-1);
    quicksort(A,q+1,r);
  }
}

