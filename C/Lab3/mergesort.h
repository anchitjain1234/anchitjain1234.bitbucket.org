#include <stdio.h>
#include "header.h"
#include <stdlib.h>
void merge(node *A,int p,int q,int r)
{
  int n1,n2;
  n1=q-p+1;
  n2=r-q;
  int l[n1],m[n2];
  int i,j=0,k=0,f=0,g=0;
  // f and g are flags to tell whether l and m arrays have been checked completely.
  
  //printf("l array is\n");
  for(i=0;i<n1;i++)
  {
    node *a;
    a=jump(A,p+i);
    l[i]=a->i;
    //printf("%d\n",l[i]);
  }
  //printf("m array is\n");
  for(i=0;i<n2;i++)
  {
    node *a;
    a=jump(A,q+i+1);
    m[i]=a->i;
    //printf("%d\n",m[i]);
  }

  for(i=p;i<=r;i++)
  {
    if(f==1)
    {
      node *a;
      a=jump(A,i);
      //printf("f=1 for %d\n",a->i);
      a->i=m[k];
      k++;
      continue;
    }
    if(g==1)
    {
      node *a;
      a=jump(A,i);
      //printf("g=1 for %d\n",a->i);
      a->i=l[j];
      j++;
      continue;
    }
    if(l[j]<=m[k])
    {
      node *a;
      a=jump(A,i);
      //printf("l[%d]=%d and m[%d]=%d\n",j,l[j],k,m[k]);
      a->i=l[j];
      //printf("a->%d becomes %d\n",i,a->i);
      j++;
      if(j==n1)
        f=1;
    }
    else
    {
      node *a;
      a=jump(A,i);
      //printf("l[%d]=%d and m[%d]=%d\n",j,l[j],k,m[k]);
      a->i=m[k];
      //printf("a->%d becomes %d\n",i,a->i);
      k++;
      if(k==n2)
        g=1;
    }
  }
  //printf("list after merge for p=%d,q=%d,r=%d\n",p,q,r);
}

void mergesort(node *A,int p,int r)
{
  if(p<r)
  {
    int q=(p+r)/2;
    mergesort(A,p,q);
    mergesort(A,q+1,r);
    merge(A,p,q,r);
  }
}


