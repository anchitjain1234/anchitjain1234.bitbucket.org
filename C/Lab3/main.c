#include "header.h"
#include "quicksort.h"
#include "mergesort.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//conio.h isn't required.
node *head;


void printdata(node *A,char file[])
{
  node *temp=A;
  FILE *fp;
  fp=fopen("l.txt","w");
  char num[20];
  while(temp)
  {
    fprintf(fp,"%d\n",temp->i);
    temp=temp->next;
  }
  
  fclose(fp);
  
}

int main(int argc, char *argv[])
{
  FILE *fp;
  node *temp,*h2=NULL;
  char file[100],num[10];
  strcpy(file, argv[1]);
  int n,i=0,j;
  fp=fopen(file,"rt");

  if(fp==NULL)
  {
    printf("Error in opening %s file\n",file);
  }
  else
  {
    while(fgets(num,10,fp)!=NULL)
    {
      temp = (node *)malloc(sizeof(node));
      sscanf(num,"%d",&(temp->i));
      temp->next=NULL;
      if (i==0)
      {
        head=temp;
        h2=head;
      }
      else
      {
        h2->next=temp;
      }
      h2=temp;
      i++;
    }
  }
  fclose(fp);
  printlist(head);
  
  while(1)
  {
  	printf("Enter 1 for quicksort,0 for mergesort\n");
  	int r;
  	scanf("%d",&r);
  	if(r==0)
  	{
  		printf("\nMerge Sort chosen\n");
  		mergesort(head,0,i-1);
  		break;
  	}
  	else if(r==1)
  	{
  		printf("\nQuick Sort chosen\n");
  		quicksort(head,0,i-1);
  		break;
  	}
  }
  
  //mergesort(head,0,i-1);
  //printlist(head);
  printdata(head,file);
  printf("\nCheck %s for sorted elements\n\n\n",file);

  return 0;
}
