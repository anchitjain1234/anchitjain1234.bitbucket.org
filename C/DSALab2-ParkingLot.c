#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct 
{
	int regno;
	char name[100];
}car;

typedef struct 
{
	car c;
	char parkingid[110];
}parkinglot;

void parking_snapshot(parkinglot *plot,int count)
{
	printf("\n\n\nDetails of cars parked are\n\n");
	printf("Reg No.   Owner Name          Parking Id\n");
	int i;
	for (i = 0; i < count; ++i)
	{
		printf("%d         %s      %s\n",plot[i].c.regno,plot[i].c.name,plot[i].parkingid );
	}

}

int exit_parkinglot(parkinglot *plot,int count)
{
	if (count>0)
	{
		count-=1;
	}
	else
	{
		printf("Stack is already empty\n");
	}
	return count;
}

int enter_parkinglot(parkinglot *plot,int count,int n)
{
	if (count<n)
	{
		parkinglot lott;
		char n1[]="owner of car ";
		char n2[6];
		sprintf(n2,"%d",count+1);
		lott.c.regno=count+1;
		strcat(n1,n2);
		strcpy(lott.c.name,n1);
		char s[]=".";
		strcat(n1,s);
		strcat(n1,n2);
		strcpy(lott.parkingid,n1);
		plot[count]=lott;
		count+=1;
		printf("Parking Lot entered in stack.\n");
	}
	else
	{
		printf("Stack is full\n");
	}
	return count;
}

int main(int argc, char const *argv[])
{
	/* code */
	parkinglot *plot;
	int n,count=0,z;
	printf("Enter size of stack\n");
	scanf("%d",&n);
	plot = (parkinglot *)malloc(n*sizeof(parkinglot));
	while(1==1)
	{
		printf("0 to exit program,1 to enter,2 to exit,3 to find car,4 to see all\n");
		scanf("%d",&z);
		if (z==0)
		{
			break;
		}
		else if (z==1)
		{
			count=enter_parkinglot(plot,count,n);
		}
		else if(z==2)
		{
			count=exit_parkinglot(plot,count);
		}
		else if(z==4)
		{
			parking_snapshot(plot,count);
		}
		else
		{
			continue;
		}
	}
	return 0;
}