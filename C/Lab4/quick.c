#include "pivot.c"


int partition(int A[],int p,int r)
{
  int i,j,x,temp,pivot;
  i=p-1;
  pivot=pivotfirst(p,r);
  ///pivot=pivotsecond(A,p,r);
  x=A[pivot];
  for(j=p;j<=r-1;j++)
  {
    if(A[j]<=x)
    {
      i++;
      exchange(A,i,j);
    }
  }
  i++;
  exchange(A,i,pivot);
  return i;
}

void quicksort(int A[],int p,int r)
{
  int q;
  if(p<r)
  {
    q=partition(A,p,r);
    quicksort(A,p,q-1);
    quicksort(A,q+1,r);
  }
}
