#include<stdio.h>

void merge(int A[],int p,int q,int r)
{
  int n1,n2;
  n1=q-p+1;
  n2=r-q;
  int l[n1],m[n2];
  int i,j=0,k=0,f=0,g=0;
  // f and g are flags to tell whether l and m arrays have been checked completely.

  for(i=0;i<n1;i++)
  {
    l[i]=A[p+i];
  }
  for(i=0;i<n2;i++)
  {
    m[i]=A[q+i+1];
  }

  for(i=p;i<=r;i++)
  {
    if(f==1)
    {
      A[i]=m[k];
      k++;
      continue;
    }
    if(g==1)
    {
      A[i]=l[j];
      j++;
      continue;
    }
    if(l[j]<=m[k])
    {
      A[i]=l[j];
      j++;
      if(j==n1)
        f=1;
    }
    else
    {
      A[i]=m[k];
      k++;
      if(k==n2)
        g=1;
    }
  }
}

void mergesort(int A[],int p,int r)
{
  if(p<r)
  {
    int q=(p+r)/2;
    mergesort(A,p,q);
    mergesort(A,q+1,r);
    merge(A,p,q,r);
  }
}
