#include "insertionsort.c"
#include "mergesort.c"
#include "quick.c"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void file_read(int num[],int t)
{
	FILE *fp;
	char temp[15];
	int i=t;
	fp=fopen("input.txt","rt");
	if(fp==NULL)
	  {
		printf("Error in opening file\n");
	  }
	  else
	  {
		while(i>0 && fgets(temp,10,fp)!=NULL)
		{
		  sscanf(temp,"%d",&num[t-i]);
		  i--;
		}
	  }
	  
	  fclose(fp);
}

void file_write(int num[],int t)
{
  int i=0;
  FILE *fp;
  fp=fopen("result.txt","w");
  while(i<t)
  {
    fprintf(fp,"%d\n",num[i]);
    i++;
  }

  fclose(fp);
}

int main(int argc, char *argv[])
{
  int *num;
  int t=atoi(argv[1]);
  num=malloc(t*sizeof(int));
  if(num==0)
  {
  printf("memory not available");
  return 1;
  }
  file_read(num,t);
  insertionsort(num,t);
  //mergesort(num,0,t-1);
  //quicksort(num,0,t-1);
  file_write(num,t);
  free(num);
  return 0;
}
