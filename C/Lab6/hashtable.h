

struct nod{
	int cust_id;
	char item_code[8];
	int cost;
	struct nod *next;
};

typedef struct nod node;

struct hashid{
	struct nod *head;
	int flag;
};

typedef struct hashid hash_id;

struct hashitem{
	hash_id idtab[10];
};

typedef struct hashitem hash_item;

