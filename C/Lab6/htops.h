#include <stdio.h>

hash_item * populatetable(char *filename);

void printHT(hash_item *ht,int htsize,FILE *fp);

hash_item insertHashtable(hash_item ht,int htsize,int cust_id,char *item,int cost_item);

int findEntry(hash_item ht, int htsize, int cust_id, char *item_code);

int purchasedby(hash_item ht, int htsize, int cust_id);

int hash1(char *key, int size);

int hash2(int key, int size);