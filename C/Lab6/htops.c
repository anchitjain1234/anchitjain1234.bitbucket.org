#include "hashtable.h"
#include "htops.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int csize=10;
int sumofdigits(int n)
{
   int t, sum = 0, remainder;
   t = n;
 
   while (t != 0)
   {
      remainder = t % 10;
      sum       = sum + remainder;
      t         = t / 10;
   }
   return sum;
}


hash_item * populatetable(char *filename)
{
	FILE *fp;
	hash_item *hi;
	char temp[25];
	int size_t=500;
	hi=(hash_item *)malloc(sizeof(hash_item)*size_t);
	fp=fopen(filename,"r");
	int cust_id,cost_item;
	char item[8];
	if (fp!=NULL)
	{
		
		while(fscanf(fp,"%d %s %d",&cust_id,item,&cost_item)==3)
		{
			
			int h1=hash1(item,size_t);
			hi[h1]=insertHashtable(hi[h1],size_t,cust_id,item,cost_item);
			//adsasadsadasdasd
			
		}
		printHT(hi,size_t,fp);
	}

}

void printHT(hash_item *ht,int htsize,FILE *fp)
{
	int i,j;
	for ( j = 0; i < htsize; ++j)
	{
		for ( i = 0; i < 10; ++i)
		{
			if (ht[j].idtab[i].head!=NULL)
			{
				char a[8];

				strcpy(a,ht[j].idtab[i].head->item_code);
				printf("%c %c\n",a[0],a[7] );
				printf("%d %s %d\n",ht[j].idtab[i].head->cust_id,a,ht[j].idtab[i].head->cost );
			}
			
		}
	}
	
}

hash_item insertHashtable(hash_item ht,int htsize,int cust_id,char *item,int cost_item)
{
	
	int h2=hash2(cust_id,csize);
	 node *n1;
	 n1=(node *)malloc(sizeof(node));
	n1->cust_id=cust_id;
	strcpy(n1->item_code,item);
	n1->cost=cost_item;
	if (ht.idtab[h2].flag==0)
	{
		ht.idtab[h2].flag=1;
		ht.idtab[h2].head=NULL;
	}
	n1->next=ht.idtab[h2].head;
	ht.idtab[h2].head=n1;
	return ht;
}

int findEntry(hash_item ht, int htsize, int cust_id, char *item_code);

int purchasedby(hash_item ht, int htsize, int cust_id);

int hash1(char *key, int size)
{
	int t=strlen(key);
	int i,p=503,asciisum=0;
	for(i=0;i<t;i++)
	{
		int j=key[i];
		asciisum+=j;
	}
	int hash=((asciisum%p)%size);
	return hash;
}

int hash2(int key, int size)
{
	int sum=sumofdigits(key);
	int hash=sum%csize;
	return hash;
}

int main(int argc, char const *argv[])
{
	char filename[20];
	strcpy(filename,"inputHashing.txt");
	hash_item *hi;
	int size_t=500;
	hi=(hash_item *)malloc(sizeof(hash_item)*size_t);

	hi=populatetable(filename);
	
	return 0;
}