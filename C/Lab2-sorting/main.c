#include "parkinglot.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int count=0;
void parking_snapshot(parkinglot *plot)
{
	printf("\n\n\nDetails of cars parked are\n\n");
	printf("Reg No.   Owner Name          Parking Id\n");
	int i;
	for (i = 0; i < count; ++i)
	{
		printf("%d         %s      %d\n",plot[i].c.regno,plot[i].c.name,plot[i].parkingid );
	}

}

void insertionsort(parkinglot *plot)
{
	parkinglot t;
	int i,d;
	for(i=1;i<=count-1;i++)
{
	d=i;
while ( d > 0 && plot[d].c.regno < plot[d-1].c.regno) {
      t          = plot[d];
      plot[d]   = plot[d-1];
      plot[d-1] = t;
 
      d--;
    }
}
printf("\n\nInsertion Sort performed.\n");
}

void partition();
void mergeSort();


void enter_parkinglot(parkinglot *plot,int n)
{
        cname ca[10];
        strcpy(ca[0].carname,"bmw owner ");
        strcpy(ca[1].carname,"mercedes owner ");
        strcpy(ca[2].carname,"maruti owner ");
        strcpy(ca[3].carname,"toyota owner ");
        strcpy(ca[4].carname,"hyundai owner ");
        strcpy(ca[5].carname,"chevrolet owner ");
        strcpy(ca[6].carname,"audi owner ");
        strcpy(ca[7].carname,"volkswagon owner ");
        strcpy(ca[8].carname,"ford owner ");
        strcpy(ca[9].carname,"jaguar owner ");
        int r=rand()%10;
	if (count<n)
	{
		parkinglot lott;
		char n1[30];
		strcpy(n1,ca[r].carname);
		char n2[6];
		lott.c.regno=rand()%1000;
		sprintf(n2,"%d",lott.c.regno);
		strcat(n1,n2);
		strcpy(lott.c.name,n1);
		lott.parkingid=count+1;
		plot[count]=lott;
		count+=1;
		printf("Car entered in parking lot.\n");
	}
	else
	{
		printf("Parking lot is full\n");
	}
}

int main(int argc, char *argv[])
{
	int n;
	if(argc<2)
	{
		printf("Please enter arguments while compiling.\n");
		return 0;
	}
	else
	{
		n=atoi(argv[1]);
	}
	
	parkinglot *plot;

	plot = (parkinglot *)malloc(n*sizeof(parkinglot));
	int z;
	while(1==1)
	{
		printf("1 to enter car,2 to print cars entered,3 for insertion sort,4 for merge sort\n");
		scanf("%d",&z);
		if (z==1)
		{
			enter_parkinglot(plot,n);
		}
		else if(z==2)
		{
			
			parking_snapshot(plot);
		}
		else if(z==3)
		{
		        insertionsort(plot);
		}
		else if(z==4)
		{
		        partition(plot,0,count-1);
		        printf("\n\nMerge Sort performed.\n");
		}
		else continue;
	}

	return 0;
}


 void partition(parkinglot arr[],int low,int high){

    int mid;

    if(low<high){
         mid=(low+high)/2;
         partition(arr,low,mid);
         partition(arr,mid+1,high);
         mergeSort(arr,low,mid,high);
    }
}

void mergeSort(parkinglot arr[],int low,int mid,int high){

    int i,m,k,l;
    parkinglot temp[count];

    l=low;
    i=low;
    m=mid+1;

    while((l<=mid)&&(m<=high)){

         if(arr[l].c.regno<=arr[m].c.regno){
             temp[i]=arr[l];
             l++;
         }
         else{
             temp[i]=arr[m];
             m++;
         }
         i++;
    }

    if(l>mid){
         for(k=m;k<=high;k++){
             temp[i]=arr[k];
             i++;
         }
    }
    else{
         for(k=l;k<=mid;k++){
             temp[i]=arr[k];
             i++;
         }
    }
   
    for(k=low;k<=high;k++){
         arr[k]=temp[k];
    }
}
