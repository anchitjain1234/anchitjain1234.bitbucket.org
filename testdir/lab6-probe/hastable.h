struct hash_id{
  int custid;
  char itemcode[10];
  int itemcost;
};

typedef struct hash_id hid;

struct hash_item{
  struct hash_id hid[100];
};

typedef struct hash_item hitem;
