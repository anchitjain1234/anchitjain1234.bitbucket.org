#include "hastable.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int sizet=100;
int ct=0;
int ct2=0;
int ct3=0;
int ct4=0;
void printht(hitem *ht,int htsize,FILE *fp)
{
	int i,j;
	for ( i = 0; i < htsize; ++i)
	{
		for ( j = 0; j < sizet; ++j)
		{
			if (ht[i].hid[j].custid!=0)
			{
				ct2++;
				fprintf(fp,"%d %s %d\n",ht[i].hid[j].custid,ht[i].hid[j].itemcode,ht[i].hid[j].itemcost );
			}
			
		}
	}
}

hitem inserthashtable(hitem ht, int htsize, int cust_id, char *item_code, int cost_item)
{
	int h2;
	h2=hash2lin(cust_id,sizet,ht);
	ht.hid[h2].custid=cust_id;
	strcpy(ht.hid[h2].itemcode,item_code);
	ht.hid[h2].itemcost=cost_item;
	ct++;
	return ht;
}

hitem* populatehashtable(char *fname,char *outfile)
{
	FILE *fp;
	fp=fopen(fname,"r");

	hitem *hi;
	hi=(hitem *)malloc(1000*sizeof(hitem));

	int custid,itemcost;
	char itemcode[10];
	while(fscanf(fp,"%d %s %d",&custid,itemcode,&itemcost)==3) {
		int h1;
		h1=hash1(itemcode,1000);
	    hi[h1]=inserthashtable(hi[h1],1000,custid,itemcode,itemcost);
	}
	fclose(fp);
	printf("ct=%d\n",ct );
	fp=fopen(outfile,"w");
	printht(hi,1000,fp);
	fclose(fp);
	printf("ct3=%d\n",ct3 );
	printf("ct2=%d\n",ct2 );
	return hi;
}


int hash1(char *key, int size)
{
	int t=strlen(key);
	int i,p=503,asciisum=0;
	for(i=0;i<t;i++)
	{
		int j=key[i];
		asciisum+=j;
	}
	int hash=((asciisum%p)%size);
	return hash;
}

int sumofdigits(int n)
{
   int t, sum = 0, remainder;
   t = n;
 
   while (t != 0)
   {
      remainder = t % 10;
      sum       = sum + remainder;
      t         = t / 10;
   }
   return sum;
}

int hash2lin(int key, int size,hitem hi)
{
	int sum=sumofdigits(key);
	int hash=sum%sizet;
	int i=0;
	while (hi.hid[hash+i].custid!=0)
	{
		i++;

	}
	if(i!=0)
	{
		ct3++;
	}
	return (hash+i)%sizet;
}

int main(int argc, char const *argv[])
{
	char inpfile[50],outfile[50];
	strcpy(inpfile,argv[1]);
	strcpy(outfile,argv[2]);
	hitem *hi;
	hi=(hitem *)malloc(1000*sizeof(hitem));
	hi=populatehashtable(inpfile,outfile);
	return 0;
}


