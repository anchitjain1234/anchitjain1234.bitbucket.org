#include "functions.h"
int time;
int backedge=0;
void printstatus(node *graph,int ver)
{
	printf("nodes after dfs\n");
	int i;
	
	for(i=0;i<ver;i++)
	{
		printf("for vertice %d - parent = %d , ti = %d, tf = %d \n",i+1,graph[i].parent+1,graph[i].ti,graph[i].tf);
	}
	printf("backedges are %d\n",backedge );
	if (backedge)
	{
		printf("there is cycle present\n");
	}
	else
	{
		printf("no cycle\n");
	}
}

void dfsvisit(node *graph,int src,int ver)
{
	int i;
	time++;
	graph[src].ti=time;
	graph[src].color=2;

	normal *temp=graph[src].head;

	while(temp)
	{
		i=temp->val;

		if(graph[i].color!=1 && graph[src].parent!=i)
		{
			backedge++;
		}

		if(graph[i].color==1)
		{
			graph[i].parent=src;
			dfsvisit(graph,i,ver);
		}
		temp=temp->next;
	}


	graph[src].color=0;
	time++;
	graph[src].tf=time;
}

void dfs(node *graph,int ver)
{
  int i;
  for(i=0;i<ver;i++)
  {
  	graph[i].color=1;
  	graph[i].parent=-1;
  }
  time=0;

  for(i=0;i<ver;i++)
  {
  	if(graph[i].color==1)
  	{
  		dfsvisit(graph,i,ver);
  	}
  }
  printstatus(graph,ver);
}
