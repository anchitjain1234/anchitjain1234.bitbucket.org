#include "functions.h"

node* creategraph(node *graph,char* file)
{
  int ver,t1,t2;
  FILE *fp;

  fp=fopen(file,"r");

  fscanf(fp,"%d",&ver);

  graph=(node *)malloc(ver*sizeof(node));
  
  int i;
  for(i=0;i<ver;i++)
  {
    graph[i].head=NULL;
  }
  normal *temp1,*temp2;

  while(fscanf(fp,"%d %d",&t1,&t2)==2)
  {
    
    temp1=(normal *)malloc(1*sizeof(normal));
    temp2=(normal *)malloc(1*sizeof(normal));
    temp1->val=t2-1;
    temp1->next=graph[t1-1].head;
    temp2->val=t1-1;
    temp2->next=graph[t2-1].head;

    graph[t1-1].head=temp1;
    graph[t2-1].head=temp2;
  }
  return graph;
}
