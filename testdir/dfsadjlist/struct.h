#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct norm{
	int val;
	struct norm* next;
};

typedef struct norm normal;

struct nod{
  normal *head;
  int color;
  int parent;
  int ti;
  int tf;
};
typedef struct nod node;
