


#ifndef HASTABLE_H
#define HASTABLE_H
#include <stdio.h>
#include <stdlib.h>
struct nod{
  int custid;
  char itemcode[10];
  int itemcost;
  struct nod *next;
};

typedef struct nod node;

struct hash_id{
  struct nod *head;
};

typedef struct hash_id hid;

struct hash_item{
  struct hash_id hid[100];
};

typedef struct hash_item hitem;
#endif /* HASTABLE_H */