#include "htops.h"
#include <string.h>
int sizet=100;

hitem* populatehashtable(char *fname,char *outfile)
{
	FILE *fp;
	fp=fopen(fname,"r");

	hitem *hi;
	hi=(hitem *)malloc(1000*sizeof(hitem));

	int custid,itemcost;
	char itemcode[10];
	while(fscanf(fp,"%d %s %d",&custid,itemcode,&itemcost)==3) {
		int h1;
		h1=hash1(itemcode,1000);
	    hi[h1]=inserthashtable(hi[h1],1000,custid,itemcode,itemcost);
	}
	fclose(fp);
	fp=fopen(outfile,"w");
	printht(hi,1000,fp);
	fclose(fp);
	return hi;
}

void printht(hitem *ht,int htsize,FILE *fp)
{
	int i,j;
	for ( i = 0; i < htsize; ++i)
	{
		for ( j = 0; j < sizet; ++j)
		{
			node *f=ht[i].hid[j].head;
			while(f!=NULL) 
			{
				//printf("%d %s %d\n",f->custid,f->itemcode,f->itemcost );
			    fprintf(fp, "%d %s %d\n",f->custid,f->itemcode,f->itemcost);
			    f=f->next;
			}
		}
	}
}

hitem inserthashtable(hitem ht, int htsize, int cust_id, char *item_code, int cost_item)
{
	int h2;
	h2=hash2(cust_id,sizet);

	node *temp;
	temp=(node *)malloc(sizeof(node));
	temp->custid=cust_id;
	strcpy(temp->itemcode,item_code);
	temp->itemcost=cost_item;
	temp->next=ht.hid[h2].head;
	ht.hid[h2].head=temp;
	return ht;
}

int findEntry(hitem* ht, int htsize, int cust_id, char *item_code)
{
	int h1,h2;
	h1=hash1(item_code,1000);
	h2=hash2(cust_id,sizet);
	node *n=ht[h1].hid[h2].head;
	while(n!=NULL) 
	{
	    printf("%d\n",n->itemcost );
	    n=n->next;	
	}
}

int purchasedby(hitem* ht, int htsize,int cust_id)
{
	int i,j,h2,res=0;
	h2=hash2(cust_id,sizet);
	for ( i = 0; i < 1000; ++i)
	{
		node *n=ht[i].hid[h2].head;
		while(n!=NULL) 
		{
			if (n->custid==cust_id)
			{
			    res+=n->itemcost;
			}
			n=n->next;
		}
		
	}
	printf("%d\n",res );
}
int hash1(char *key, int size)
{
	int t=strlen(key);
	int i,p=503,asciisum=0;
	for(i=0;i<t;i++)
	{
		int j=key[i];
		asciisum+=j;
	}
	int hash=((asciisum%p)%size);
	return hash;
}

int sumofdigits(int n)
{
   int t, sum = 0, remainder;
   t = n;
 
   while (t != 0)
   {
      remainder = t % 10;
      sum       = sum + remainder;
      t         = t / 10;
   }
   return sum;
}

int hash2(int key, int size)
{
	int sum=sumofdigits(key);
	int hash=sum%sizet;
	return hash;
}



