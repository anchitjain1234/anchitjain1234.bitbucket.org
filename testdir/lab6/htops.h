#include "hastable.h"

hitem * populatehashtable(char * filename,char *outfile);

void printht(hitem *ht,int htsize,FILE *fp);

hitem inserthashtable(hitem ht, int htsize, int cust_id, char *item_code, int cost_item);

int findEntry(hitem* ht, int htsize, int cust_id, char *item_code);

int purchasedby(hitem* ht, int htsize,int cust_id);

int hash1(char *key, int size);

int hash2(int key,int size);
