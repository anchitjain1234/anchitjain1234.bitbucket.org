#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct edg{
	int s;
	int d;
	int wt;
};

typedef struct edg edge;