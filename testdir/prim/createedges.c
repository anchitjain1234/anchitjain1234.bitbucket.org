#include "functions.h"

edge* createedges(edge * ed,char *file,int *ot)
{
	FILE *fp;
	fp=fopen(file,"r");
	int ver,edg,t1,t2,t3,ct=0;
	fscanf(fp,"%d %d",&ver,&edg);

	ot[0]=ver;
	ot[1]=edg;

	ed=(edge *)malloc(edg*sizeof(edge));

	while(fscanf(fp,"%d %d %d",&t1,&t2,&t3)==3)
	{
		ed[ct].s=t1-1;
		ed[ct].d=t2-1;
		ed[ct].wt=t3;
		ct++;
	}
	return ed;

}