#include "functions.h"

#define inf 10000000

int findmin(int *key,int *mst,int ver)
{
	int min=inf,min_index;
	int i;

	for(i=0;i<ver;i++)
	{
		if(mst[i]==0 && min>key[i] && key[i] !=inf)
		{
			min=key[i];
			min_index=i;
		}
	}
	return min_index;
}

void printprim(int *parent,int **graph,int ver)
{
	printf("Edge   Weight\n");
	int i;
   	for (i = 0; i < ver; i++)
   	{
   		printf("%d - %d    %d \n", parent[i], i, graph[i][parent[i]]);
   	}
   	
}

void prim(int **graph,int ver)
{
	int *mst,*parent,*key;
	int i,j;

	mst=(int *)malloc(ver*sizeof(int));
	parent=(int *)malloc(ver*sizeof(int));
	key=(int *)malloc(ver*sizeof(int));

	for(i=0;i<ver;i++)
	{
		key[i]=inf;
		mst[i]=0;
	}

	key[0]=0;
	parent[0]=-1;

	for(j=0;j<(ver-1);j++)
	{
		int u=findmin(key,mst,ver);

		mst[u]=1;

		for(i=0;i<ver;i++)
		{
			if(graph[u][i] && graph[u][i]<key[i] && mst[i]==0)
			{
				parent[i]=u;
				key[i]=graph[u][i];
			}
		}
	}
	printprim(parent,graph,ver);
}

