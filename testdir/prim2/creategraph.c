#include "functions.h"

int** creategraph(int **graph,char *file,int *ot)
{
  FILE* fp;
  fp=fopen(file,"r");
  int ver,i,j,t1,t2,t3;
  fscanf(fp,"%d",&ver);
  *ot =ver;
  graph=(int**)malloc(ver*sizeof(int *));
  for(i=0;i<ver;i++)
  {
    graph[i]=(int*)malloc(ver*sizeof(int ));
    for(j=0;j<ver;j++)
    {
      graph[i][j]=0;
    }
  }


  while(fscanf(fp,"%d %d %d",&t1,&t2,&t3)==3)
  {
    graph[t1-1][t2-1]=t3;
    graph[t2-1][t1-1]=t3;
  }
  return graph;
}
