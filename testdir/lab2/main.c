#include "parkinglot.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int findlength(plot *pl)
{
	int i=0;
	plot *te=pl;
	while(te!=NULL)
	{
		i+=1;
		te=te->next;
	}
	return i;
}

plot* jump(plot *hd,int i)
{
	plot *te=hd;
	while(i--) 
	{
	    te=te->next;
	}
	return te;
}

plot* enterplot(plot *pl)
{
	plot* temp1;
	int reg;
	char name[100];
	FILE *fp;
	fp=fopen("inp.txt","r");
	int i=1;
	while(fscanf(fp,"%d %s",&reg,name)==2) 
	{
	    temp1=(plot *)malloc(sizeof(plot));
	    temp1->cars.regno=reg;
	    strcpy(temp1->cars.name,name);
	    temp1->pid=i;
	    i++;
	    temp1->next=pl;
	    pl=temp1;
	}
	fclose(fp);
	return pl;
	
}

void inssortcars(plot *pl)
{
	int i,j,k;
	int l;
	l=findlength(pl);
	for (i = 1; i < l; ++i)
	{
		k=jump(pl,i)->cars.regno;
		j=i-1;
		while(j>=0 && jump(pl,j)->cars.regno<k) {
		    jump(pl,j+1)->cars.regno=jump(pl,j)->cars.regno;
		    j--;
		}
		jump(pl,j+1)->cars.regno=k;
	}
}

int partition(plot *pl,int p,int r)
{
	int x=jump(pl,r)->cars.regno;
	int i=p-1;
	int j=p;
	while(j<=r-1) 
	{
	    if (jump(pl,j)->cars.regno<=x)
	    {
	    	i++;
	    	int t=jump(pl,j)->cars.regno;
	    	jump(pl,j)->cars.regno=jump(pl,i)->cars.regno;
	    	jump(pl,i)->cars.regno=t;
	    }
	    j++;
	}
	jump(pl,r)->cars.regno=jump(pl,i+1)->cars.regno;
	jump(pl,i+1)->cars.regno=x;
	return i+1;
}

void quicksortcars(plot *pl,int p,int r)
{
	int q;
	if (p<r)
	 {
	 	q=partition(pl,p,r);
	 	quicksortcars(pl,p,q-1);
	 	quicksortcars(pl,q+1,r);
	 } 
}

void printparked(plot *pl)
{
	FILE *fp;
	fp=fopen("result.txt","w");
	plot *te;
	te=pl;
	while(te !=NULL) 
	{
	    fprintf(fp, "%d   %s\n",te->cars.regno,te->cars.name);
	    te=te->next;
	}
	fclose(fp);
}

int main(int argc, char const *argv[])
{
	plot *pl;
	pl=enterplot(pl);
	printparked(pl);
	int n=findlength(pl);
	quicksortcars(pl,0,n-1);
	printparked(pl);
	return 0;
}