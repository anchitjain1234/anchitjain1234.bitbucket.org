#include "struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int size=100;

int snapshot(plot *pl,int st)
{
	int i;
	FILE *fp;
	fp=fopen("snapshot.txt","w");

	if (fp!=NULL)
	{
		for ( i = 0; i < st; ++i)
		{
			fprintf(fp,"%d  %s   %d\n",pl[i].pid,pl[i].cars.name,pl[i].cars.regno);
		}
		fclose(fp);
		return 0;
	}
	fclose(fp);
	return -1;
}

plot isparked(plot *pl,int pid,int st)
{
	int i;
	for ( i = 0; i < st; ++i)
	{
		if (pl[i].pid==pid)
		{
			return pl[i];
		}
	}
}

int enterplot(plot *pl,int st)
{
	char name[100];
	if (st<size)
	{
		printf("Enter regno and name\n");
		scanf("%d %s",&pl[st].cars.regno,pl[st].cars.name);
		pl[st].pid=st+1;
		st+=1;
		return st;
	}
	return -1;
}

int exitplot(int st)
{
	if (st>0)
	{
		st-=1;
		return st;
	}
	return -1;
}
int main(int argc,char const *argv[])
{
	plot * pl;
	pl=(plot *)malloc(size*sizeof(plot));
	int st=0;
	int n=1;
	while(n!=0) 
	{
	    printf("Enter 1 to enter car,2 to exit,3 to snapshot,4 to exit program\n");
	    scanf("%d",&n);

	    if (n==1)
	    {
	    	st=enterplot(pl,st);
	    	if (st==-1)
	    	{
	    		printf("Overflow error\n");
	    	}
	    }

	    if (n==2)
	    {
	    	st=exitplot(st);
	    	if (st==-1)
	    	{
	    		printf("Underflow error\n");
	    	}
	    }

	    if (n==3)
	    {
	    	int b;
	    	b=snapshot(pl,st);
	    	if (b==-1)
	    	{
	    		printf("Error in writing snapshot.\n");
	    	}
	    }
	    if (n==4)
	    {
	    	exit(0);
	    }

	}
	return 0;
}