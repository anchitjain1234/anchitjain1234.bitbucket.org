#include "function.h"

node* jump(node *n,int *level)
{
	node *te=n;
	int i=*level-1;
	if(i<0)
	{
		printf("Underflow error\n");
		exit(0);
	}
	while(i--)
	{
		te=te->next;
	}
	return te;
}

void push(node **n,int *level)
{
	int val;
	printf("Enter value\n");
	scanf("%d",&val);
	node *te;
	te=(node *)malloc(sizeof(node));
	te->val=val;
	if(*n==NULL)
	{
		*n=te;
	}
	else
	{
		jump(*n,level)->next=te;
	}
	*level+=1;
	
}

void pop(node **n,int *level)
{
	*level-=1;
	jump(*n,level)->next=NULL;
}
