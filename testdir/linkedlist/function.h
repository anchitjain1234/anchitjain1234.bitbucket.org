#include "struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void createlist(node **n,char *file);
void printlist(node *n,char *file);
void push(node **n,int *level);
void pop(node **n,int *level);
node* jump(node *n,int *level);
