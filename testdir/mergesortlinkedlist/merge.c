#include "struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void prinlist(node *f);

node* jump(node *f,int ct)
{
	node *t=f;
	while(ct--) 
	{
	    t=t->next;
	}
	return t;
}

void merge(node *f,int n1,int mid,int n2)
{
	node *h1=NULL,*h2=NULL,*h=f;
	int v,i,p,q,f1,g1;
	f1=0;
	g1=0;
	p=mid-n1+1;
	q=n2-mid;
	node *t1,*te2;
	for (i = 0; i < p; ++i)
	{
		t1=jump(f,n1+i);
		te2=(node *)malloc(sizeof(node));
		te2->val=t1->val;
		te2->next=NULL;
		node *head;
		if(i==0)
		{
			h1=te2;
			head=h1;
		}
		else
		{
			head->next=te2;  
		}
		head=te2;
	}
	for (i = 0; i < q; ++i)
	{
		t1=jump(f,mid+i+1);
		te2=(node *)malloc(sizeof(node));
		te2->val=t1->val;
		te2->next=NULL;
		node *head;
		if(i==0)
		{
			h2=te2;
			head=h2;
		}
		else
		{
			head->next=te2;  
		}
		head=te2;
	}
	t1=h1;
	te2=h2;
	for ( i = n1; i <= n2; ++i)
	{
		if (f1==1)
		{
			node *a;
      		a=jump(f,i);
      		a->val=te2->val;
      		te2=te2->next;
      		continue;
		}

		if (g1==1)
		{
			node *a;
      		a=jump(f,i);
      		a->val=t1->val;
      		t1=t1->next;
      		continue;
		}

		if (t1->val<=te2->val)
		{
			node *a;
      		a=jump(f,i);
      		a->val=t1->val;
      		t1=t1->next;
      		if (t1==NULL)
      		{
      			f1=1;
      		}
		}
		else
		{
			node *a;
      		a=jump(f,i);
      		a->val=te2->val;
      		te2=te2->next;
      		if (te2==NULL)
      		{
      			g1=1;
      		}
		}
	}
}

void mergesort(node *f,int n1,int n2)
{
	if (n2>n1)
	{
		int mid=(n2+n1)/2;
		mergesort( f,n1,mid);
		mergesort( f,mid+1,n2);
		merge(f,n1,mid,n2);
	}
}

int main(int argc, char const *argv[])
{
	FILE *fp;
	fp=fopen("input.txt","r");
	int t,n=0;
	node *temp,*head=NULL;
	
	while(fscanf(fp,"%d\n",&t)==1) 
	{
		temp=(node *)malloc(sizeof(node));
	    temp->val=t;
	    temp->next=head;
	    head=temp;
	    n++;
	}
	prinlist(head);
	mergesort(head,0,n-1);
	printf("\n");
	prinlist(head);
	return 0;
}

void prinlist(node *f)
{
	node *t=f;
	while(t!=NULL)
	{
		printf("%d\n",t->val );
		t=t->next;
	}
}