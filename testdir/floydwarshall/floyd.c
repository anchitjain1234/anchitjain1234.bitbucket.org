#include "functions.h"

void floyd(int **graph,int ver)
{
	int **dist,k,i,j;

	dist=(int **)malloc(ver*sizeof(int *));

	for ( i = 0; i < ver; i++)
	{
		dist[i]=(int *)malloc(ver*sizeof(int));
		for(j=0;j<ver;j++)
		{
			dist[i][j]=graph[i][j];
		}
	}

	for(k=0;k<ver;k++)
	{
		for(i=0;i<ver;i++)
		{
			for(j=0;j<ver;j++)
			{
				if(dist[i][k]+dist[k][j]<dist[i][j])
				{
					dist[i][j]=dist[i][k]+dist[k][j];
				}
			}
		}
	}

	print(dist,ver);

}