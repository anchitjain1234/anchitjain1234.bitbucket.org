#include "functions.h"
#define inf 1000000
void print(int **graph,int ver)
{
	printf("Matrix \n");

	int i,j;
	for(i=0;i<ver;i++)
	{
		for(j=0;j<ver;j++)
		{
			if(graph[i][j]==inf)
			{
				printf("%d ",-1);
			}
			else
			{
				printf("%d ",graph[i][j]);
			}
			
		}
		printf("\n");
	}

	printf("\n\n");
}