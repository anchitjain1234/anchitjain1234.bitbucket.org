#include "maze.h"

int createmaze(maze *pm,char *file)
{
	FILE *fp;
	fp=fopen(file,"r");
	int n=findn(file);

	int **arr;
	arr=(int **)malloc(n*sizeof(int *));
	int i,j;
	for ( i = 0; i < n; ++i)
	{
		arr[i]=(int *)malloc(n*sizeof(int));
		for ( j = 0; j < n; ++j)
		{
			fscanf(fp,"%d",&arr[i][j]);
			//printf("arr[%d][%d]=%d\n",i,j,arr[i][j] );
		}
	}
	pm->a=arr;
	return n;
}

int findn(char *file)
{
	FILE *fp;
	fp=fopen(file,"r");
	char str[100];
	fgets(str,100,fp);
	int t=strlen(str);
	int i,n=0;

	for (i = 0; i < t-1; i+=2)
	{
		n++;
	}
	fclose(fp);
	return n;
}
