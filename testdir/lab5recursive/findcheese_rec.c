#include "maze.h"

void createpath(path **p,int x,int y)
{
	path *temp,*curr;
	temp=(path *)malloc(sizeof(path));
	temp->x=x;
	temp->y=y;
	temp->next=*p;
	*p=temp;
	
}

boolean findcheese(maze *m, int posi, int posj, path **path_so_far, int past_i, int past_j,int n)
{
	
	int y=0,u=0,i=0,o=0;
	if(m->a[posi][posj]==9)
	{
		/*
		m->a[posi][posj]=-1;
		
		*/
		//
		createpath(path_so_far,posi,posj);
		m->a[posi][posj]=-1;
		return true;
	}
	
	if(posi-1>=0 && (m->a[posi-1][posj]==0 || m->a[posi-1][posj]==9) && (posi-1!=past_i || posj!=past_j) && m->a[posi-1][posj]!=-1)
	{
		y=1;
		/*
		if(m->a[posi-1][posj]!=9)
		{m->a[posi-1][posj]=-1;}
		*/
		m->a[posi][posj]=-1;
		boolean t=findcheese(m,posi-1,posj,path_so_far,posi,posj,n);
		if(t==true)
		{
			createpath(path_so_far,posi,posj);
			return true;
		}
	}
	
	if(posi+1<n && (m->a[posi+1][posj]==0 || m->a[posi+1][posj]==9) && (posi+1!=past_i || posj!=past_j) && m->a[posi+1][posj]!=-1)
	{
		u=1;
		/*
		if(m->a[posi+1][posj]!=9)
		{m->a[posi+1][posj]=-1;}
		*/
		m->a[posi][posj]=-1;
		boolean t=findcheese(m,posi+1,posj,path_so_far,posi,posj,n);
		if(t==true)
		{
			createpath(path_so_far,posi,posj);
			return true;
		}
	}
	
	if(posj-1>=0 && (m->a[posi][posj-1]==0 || m->a[posi][posj-1]==9) && (posi!=past_i || posj-1!=past_j) && m->a[posi][posj-1]!=-1)
	{
		i=1;
		/*
		if(m->a[posi][posj-1]!=9)
		{m->a[posi][posj-1]=-1;}
		*/
		m->a[posi][posj]=-1;
		boolean t=findcheese(m,posi,posj-1,path_so_far,posi,posj,n);
		if(t==true)
		{
			createpath(path_so_far,posi,posj);
			return true;
		}
	}
	
	if(posj+1<n && (m->a[posi][posj+1]==0 || m->a[posi][posj+1]==9) && (posi!=past_i || posj+1!=past_j) && m->a[posi][posj+1]!=-1)
	{
		o=1;
		/*
		if(m->a[posi][posj+1]!=9)
		{m->a[posi][posj+1]=-1;}
		*/
		m->a[posi][posj]=-1;
		boolean t=findcheese(m,posi,posj+1,path_so_far,posi,posj,n);
		if(t==true)
		{
			createpath(path_so_far,posi,posj);
			return true;
		}
	}
	
	//if(y==0 && u==0 && i==0 && o==0)
	//{
		return false;
	//}
	//else{
	//return true;
	//}
}
