#include "maze.h"

void printpath(path **path_so_far)
{
	FILE *fp;
	fp=fopen("path.txt","w");
	path *t=*path_so_far;
	while(t!=NULL)
	{
		fprintf(fp,"%d %d \n",(t->x),(t->y));
		t=t->next;
	}
}

int main(int argc, char const *argv[])
{
	char fname[100];
	strcpy(fname,argv[1]);
	maze *m;
	m=(maze *)malloc(sizeof(maze));
	int n=createmaze(m,fname);
	path **path_so_far;
	path_so_far=(path **)malloc(sizeof(path *));
	boolean b=findcheese(m,0,0,path_so_far,-10,-10,n);
	if(b==true)
	{
		printf("Cheese found.\n");
		printpath(path_so_far);
	}
	else
	{
		printf("Cheese not found.\n");
	}
	
	return 0;
}
