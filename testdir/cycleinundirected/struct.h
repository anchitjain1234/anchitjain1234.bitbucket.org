#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct nod{
  int val;
  int color;
  int parent;
  int ti;
  int tf;
};
typedef struct nod node;
