#include "functions.h"

node** creategraph(node **graph,char* file)
{
  int ver,i,j,t1,t2;
  FILE *fp;

  fp=fopen(file,"r");

  fscanf(fp,"%d",&ver);

  graph=(node **)malloc(ver*sizeof(node *));

  for(i=0;i<ver;i++)
  {
    graph[i]=(node *)malloc(ver*sizeof(node));
    for(j=0;j<ver;j++)
    {
      graph[i][j].val=0;
    }
  }

  while(fscanf(fp,"%d %d",&t1,&t2)==2)
  {
    graph[t1-1][t2-1].val=1;
    graph[t2-1][t1-1].val=1;
  }
  return graph;
}
