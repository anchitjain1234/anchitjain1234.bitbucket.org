#include "functions.h"
int time;
int backedge=0;
void printstatus(node **graph,int ver)
{
	printf("nodes after dfs\n");
	int i;
	
	for(i=0;i<ver;i++)
	{
		printf("for vertice %d - parent = %d , ti = %d, tf = %d \n",i+1,graph[i][0].parent+1,graph[i][0].ti,graph[i][0].tf);
	}
	printf("backedges are %d\n",backedge );
	if (backedge)
	{
		printf("there is cycle present\n");
	}
	else
	{
		printf("no cycle\n");
	}
}

void dfsvisit(node **graph,int src,int ver)
{
	int i;
	time++;
	graph[src][0].ti=time;
	graph[src][0].color=2;

	for(i=0;i<ver;i++)
	{
		if(graph[src][i].val && graph[i][0].color!=1 && graph[src][0].parent!=i)
		{
			backedge++;
			//printf("backedge for src=%d ,i=%d\n",src+1,i+1 );
		}
		if(graph[src][i].val && graph[i][0].color==1)
		{
			graph[i][0].parent=src;
			dfsvisit(graph,i,ver);
		}
	}

	graph[src][0].color=0;
	time++;
	graph[src][0].tf=time;
}

void dfs(node **graph,int ver)
{
  int i,j;
  for(i=0;i<ver;i++)
  {
  	graph[i][0].color=1;
  	graph[i][0].parent=-1;
  }
  time=0;

  for(i=0;i<ver;i++)
  {
  	if(graph[i][0].color==1)
  	{
  		dfsvisit(graph,i,ver);
  	}
  }
  printstatus(graph,ver);
}
