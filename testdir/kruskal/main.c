#include "functions.h"

int getvertices(char* filename)
{
	FILE *fp;
	fp=fopen(filename,"r");
	int ver;
	fscanf(fp,"%d",&ver);
	fclose(fp);
	return ver;
}

int main(int argc,char *argv[])
{
  char file[20];
  int i,j,ver;
  node **graph;
  strcpy(file,argv[1]);

  graph=creategraph(graph,file);
  ver=getvertices(file);
  dfs(graph,ver);
}
