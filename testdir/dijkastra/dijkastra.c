#include "functions.h"

#define max 10000000

int findmin(int *dist,int *spt,int ver)
{
  int i,min=max,min_ind;
  for(i=0;i<ver;i++)
  {
    if(spt[i]!=1 && dist[i]<min && dist[i]!=max)
    {
      min=dist[i];
      min_ind=i;
    }
  }

  return min_ind;
}

void printans(int *dist,int *parent,int ver,int src)
{
  printf("Distances from source = %d\n",src+1);
  int i;

  for(i=0;i<ver;i++)
  {
    printf("for ver = %d distance is %d\n",i+1,dist[i]);
  }

  printf("Parents array is \n");

  for(i=0;i<ver;i++)
  {
    printf("for ver = %d parent is %d\n",i+1,parent[i]);
  }

}

void dijkastra(int **graph,int ver)
{
  int *dist,*spt,*parent,i,j,src=0;
  dist=(int *)malloc(ver*sizeof(int));
  spt=(int *)malloc(ver*sizeof(int));
  parent=(int *)malloc(ver*sizeof(int));
  for(i=0;i<ver;i++)
  {
    dist[i]=max;
    spt[i]=0;
  }
  dist[src]=0;
  for(i=0;i<ver-1;i++)
  {
    int u=findmin(dist,spt,ver);
    spt[u]=1;

    for(j=0;j<ver;j++)
    {
      if(graph[u][j] && !spt[j] && dist[u]+graph[u][j]<dist[j])
      {
        dist[j]=dist[u]+graph[u][j];
        parent[j]=u+1;
      }
    }
  }

  printans(dist,parent,ver,src);
}
