#include "functions.h"

int findvertices(char *file)
{
  FILE *fp;
  fp=fopen(file,"r");
  int ver;
  fscanf(fp,"%d",&ver);
  return ver;
}

int main(int argc,char *argv[])
{
  int **graph;
  char file[20];

  if(argc!=2)
  {
    printf("Wrong format.");
    return 0;
  }

  strcpy(file,argv[1]);
  graph=creategraph(graph,file);

  int ver=findvertices(file);
  dijkastra(graph,ver);
  return 0;
}
